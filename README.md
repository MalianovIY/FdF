# FdF

Here is an implementation of the ecole42 tutorial project rendering 3D grid of landscape.

This project developed, tested, builded and compiled on iMac 27" with MacOS and tiny student graphic library 
`MinilibX` (like sdl, but oversimplified). You can find any version of this lib for any OS on github / gitlab. 
For example, [here](https://github.com/42Paris/minilibx-linux) for linux.

You can use `CMakeList.txt` or `Makefile` for build and compile.

This repo has libft. 
The libft is my own library too, but it was created as my first school project - an implementation 
of the C standard library.

After setup and compile you can write this in the bash:
```
> FdF FILE
```

The `FILE` is a 2D matrix `A[n, m]`, where `a[i, j]` contains value of hight for point with coordinate `(i, j)`.

Control:
* Rotation (Ox, Oy, Oz): arrow up/down, arrow left/right, q/e
* Moving (Ox, Oy, Oz): a/d, w/s, c/Space
* Perspective/Zoom/Height/Width: Num+/Num-, z/x, 3/4, 1/2
* Print Image, Map, Parameters, Matrix: Enter
* Return to Start position: Num Enter
* Change algorithm: Num*

See also:
* [Fractol](https://gitlab.com/MalianovIY/Fractol)
* [RTv1](https://gitlab.com/MalianovIY/RTv1)
* [RT](https://gitlab.com/MalianovIY/RT) - copy from github my teamlead [rearming](https://github.com/rearming/RT)
